import { calculateBonuses } from "./bonus-system.js";

describe('bonus-system-tests', () => {
    let app;
    
    const Standard = "Standard"
    const Premium = "Premium"
    const Diamond = "Diamond"
    const Invalid = "Invalid"
    
    test('Standard 1000',  (done) => {
        expect(calculateBonuses(Standard, 1000)).toEqual(0.05 * 1);
        done();
    });

    test('Standard 10000',  (done) => {
        expect(calculateBonuses(Standard, 10000)).toEqual(0.05 * 1.5);
        done();
    });

    test('Standard 49999',  (done) => {
        expect(calculateBonuses(Standard, 49999)).toEqual(0.05 * 1.5);
        done();
    });

    test('Standard 50000',  (done) => {
        expect(calculateBonuses(Standard, 50000)).toEqual(0.05 * 2);
        done();
    });

    test('Standard 99999',  (done) => {
        expect(calculateBonuses(Standard, 99999)).toEqual(0.05 * 2);
        done();
    });

    test('Standard 100000',  (done) => {
        expect(calculateBonuses(Standard, 100000)).toEqual(0.05 * 2.5);
        done();
    });

    test('Premium 1000',  (done) => {
        expect(calculateBonuses(Premium, 1000)).toEqual(0.1 * 1);
        done();
    });

    test('Premium 10000',  (done) => {
        expect(calculateBonuses(Premium, 10000)).toEqual(0.1 * 1.5);
        done();
    });

    test('Premium 49999',  (done) => {
        expect(calculateBonuses(Premium, 49999)).toEqual(0.1 * 1.5);
        done();
    });

    test('Premium 50000',  (done) => {
        expect(calculateBonuses(Premium, 50000)).toEqual(0.1 * 2);
        done();
    });

    test('Premium 99999',  (done) => {
        expect(calculateBonuses(Premium, 99999)).toEqual(0.1 * 2);
        done();
    });

    test('Premium 100000',  (done) => {
        expect(calculateBonuses(Premium, 100000)).toEqual(0.1 * 2.5);
        done();
    });

    test('Diamond 1000',  (done) => {
        expect(calculateBonuses(Diamond, 1000)).toEqual(0.2 * 1);
        done();
    });

    test('Diamond 10000',  (done) => {
        expect(calculateBonuses(Diamond, 10000)).toEqual(0.2 * 1.5);
        done();
    });

    test('Diamond 49999',  (done) => {
        expect(calculateBonuses(Diamond, 49999)).toEqual(0.2 * 1.5);
        done();
    });

    test('Diamond 50000',  (done) => {
        expect(calculateBonuses(Diamond, 50000)).toEqual(0.2 * 2);
        done();
    });

    test('Diamond 99999',  (done) => {
        expect(calculateBonuses(Diamond, 99999)).toEqual(0.2 * 2);
        done();
    });

    test('Diamond 100000',  (done) => {
        expect(calculateBonuses(Diamond, 100000)).toEqual(0.2 * 2.5);
        done();
    });

    test('test 900',  (done) => {
        expect(calculateBonuses(Invalid, 900)).toEqual(0);
        done();
    });
})